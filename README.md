# Description
```
lxtool v1.0

This is a helper tool for lxsplit. This tool can be
used for splitting large files into smaller files or
can be used to merge small files into one larger file.

Note:
	lxtool has been tested on ubuntu linux
```

![image](tools/menu.png)

# Usage
```
1. place your file to be split or files to be merged in
the lxtool root directory.
2. Then open terminal to the lxtool root directory and
run the following command:

Command:
	$ ./lxtool

<===== Quick Examples =====>

===> Merging a ClockworkMod backup
Rename:
	system.ext4.tar to system.ext4.tar.001
	system.ext4.tar.a to system.ext4.tar.002
	system.ext4.tar.b to system.ext4.tar.003

After renaming as above, you can run lxtool to merge the
system files into one.

===> Splitting a file
When splitting files you'll be prompted to enter the size
you want the smaller files to be in. (M)egabytes, (k)ilobytes,
and (b)ytes. Example: 20M - 20k - 20b
```

# lxtool Author
- [GameTheory](https://bitbucket.org/GameTheory-)

**Resource:**

- [lxsplit v0.2.4](https://github.com/GameTheory-/lxsplit)
